package sample;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    public Label selectUnitsWarning;

    @FXML
    private TextField numberField;

    @FXML
    private TextField resultField;

    @FXML
    private ComboBox<String> unitComboBox1;

    @FXML
    private ComboBox<String> unitComboBox2;

    /**
     * Метод вычисляет и переводит из одной ед. измерения в другую
     */
    @FXML
    void calculate() {
        if (unitComboBox1.getSelectionModel().getSelectedIndex() == -1 || unitComboBox2.getSelectionModel().getSelectedIndex() == -1){
            selectUnitsWarning.setText("Выберите единицы измерения");
            selectUnitsWarning.setVisible(true);
            return;
        } else  selectUnitsWarning.setVisible(false);
        if (numberField.getText().equals("")){
            selectUnitsWarning.setText("Вы не ввели число");
            selectUnitsWarning.setVisible(true);
            return;
        }

        selectUnitsWarning.setVisible(false);

        switch (unitComboBox1.getSelectionModel().getSelectedIndex()){
            case 0:
                switch (unitComboBox2.getSelectionModel().getSelectedIndex()){
                    case 0:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1));
                        break;
                    case 1:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) / 1000));
                        break;
                    case 2:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) / 100000));
                        break;
                    case 3:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) / 1000000));
                        break;
                }
                break;

            case 1:
                switch (unitComboBox2.getSelectionModel().getSelectedIndex()){
                    case 0:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1000));
                        break;
                    case 1:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1));
                        break;
                    case 2:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) / 100));
                        break;
                    case 3:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) / 1000));
                        break;
                }
                break;

            case 2:
                switch (unitComboBox2.getSelectionModel().getSelectedIndex()){
                    case 0:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 100000));
                        break;
                    case 1:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1000));
                        break;
                    case 2:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1));
                        break;
                    case 3:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) / 10));
                        break;
                }
                break;

            case 3:
                switch (unitComboBox2.getSelectionModel().getSelectedIndex()){
                    case 0:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1000000));
                        break;
                    case 1:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1000));
                        break;
                    case 2:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 10));
                        break;
                    case 3:
                        resultField.setText(String.valueOf(Double.parseDouble(numberField.getText()) * 1));
                        break;
                }
                break;
        }
    }

    /**
     * Очищает поля ввода и результата
     */
    @FXML
    void clear() {
        numberField.clear();
        resultField.clear();
    }
}
